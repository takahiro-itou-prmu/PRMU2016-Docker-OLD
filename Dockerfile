
FROM  ubuntu

ADD  ./files  /root

RUN  apt  update  &&  \
     apt  -y  install  git  unzip  vim  emacs

RUN  /bin/bash  -x  /root/.scripts/setup-data.sh

WORKDIR  /root/alcon2016

